﻿Public Class frmMain
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: To allow user to shop for e-books and contact the owner of program or do things that affect their membership
    Dim contactForm As frmContactUs


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Name:  frmMain_Load
        'Purpose: Sets date and time to now
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        sbrTime.Text = FormatDateTime(Now, DateFormat.LongTime)
        sbrDate.Text = FormatDateTime(Now, DateFormat.LongDate)
    End Sub

    Private Sub tbrExit_Click(sender As Object, e As EventArgs) Handles tbrExit.Click
        'Name:  tbrExit_Click
        'Purpose: Ends Program on click
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        End
    End Sub

    Private Sub tbrLogin_Click(sender As Object, e As EventArgs) Handles tbrLogin.Click
        'Name:  tbrLogin_Click
        'Purpose: Sets the icons click to mnuLogin
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        mnuEA.PerformClick()
    End Sub

    Private Sub tbrViewCart_Click(sender As Object, e As EventArgs) Handles tbrViewCart.Click
        'Name:  tbrViewCart_Click
        'Purpose: Sets the icons click to  mnuViewCart
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        mnuViewCart.PerformClick()
    End Sub

    Private Sub tmrTick_Tick(sender As Object, e As EventArgs) Handles tmrTick.Tick
        'Name:  tmrTick_Tick
        'Purpose: Is used to keep time for the longtime
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        sbrTime.Text = FormatDateTime(Now, DateFormat.LongTime)
    End Sub

    Private Sub mnuContact_Click(sender As Object, e As EventArgs) Handles mnuContact.Click
        'Name: mnuContact_Click
        'Purpose:  Opens up contact form and sets mdi parent
        'Parameters: sender As Object, e As EventArgs
        'Return: none

        Dim contactForm As frmContactUs
        contactForm = New frmContactUs
        contactForm.MdiParent = Me
        contactForm.Show()
    End Sub

    Private Sub mnuEA_Click(sender As Object, e As EventArgs) Handles mnuEA.Click
        'Name:  mnuEA_Click
        'Purpose:  Opens the login form and displays a welcome message of the persons last name on sbr
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim formLogin As New frmLogin
        formLogin.ShowDialog()
        If LoggedIn = True Then
            mnuMember.Enabled = True
            mnuShopCart.Enabled = True
            tbrViewCart.Enabled = True
            tbrLogin.Enabled = False
            mnuCA.Enabled = False
            If MemberName <> "" Then
                sbrWelcome.Text = "Welcome " & MemberName
            End If
        End If
    End Sub

    Private Sub mnuCA_Click(sender As Object, e As EventArgs) Handles mnuCA.Click
        'Name:  mnuCA_Click
        'Purpose:  Opens the Members form and displays the controls on the form to create an account
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim formMember As New frmMember
        formMember.MdiParent = Me
        mnuCA.Enabled = False
        formMember.btnCreateAccount.Visible = True
        formMember.txtPassword.Visible = True
        formMember.txtRPassword.Visible = True
        formMember.lblPassword.Visible = True
        formMember.lblRPassword.Visible = True
        formMember.Show()
    End Sub

    Private Sub mnuMakePay_Click(sender As Object, e As EventArgs) Handles mnuMakePay.Click
        'Name:  mnuMakePay_Click
        'Purpose:  Opens the Members form and displays the controls on the form to make a payment
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim formPayment As New frmMember
        formPayment.MdiParent = Me
        mnuMember.Enabled = False
        formPayment.btnMakePayment.Visible = True
        formPayment.txtPayment.Visible = True
        formPayment.txtBalance.Visible = True
        formPayment.lblPayment.Visible = True
        formPayment.lblBalance.Visible = True
        formPayment.Show()
    End Sub


    Private Sub mnuLogout_Click(sender As Object, e As EventArgs) Handles mnuLogout.Click
        'Name:  mnuLogout_Click
        'Purpose:  brings up message box with yes no button, yes sets logged in to false and disables mnuMembers and removes welcome message
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim result = MessageBox.Show("Are you sure you want to log out?", "Logout", MessageBoxButtons.YesNo)
        If (result = DialogResult.Yes) Then
            mnuMember.Enabled = False
            LoggedIn = False
            mnuCA.Enabled = True
            sbrWelcome.Text = ""
            mnuViewCart.Enabled = False
            tbrLogin.Enabled = True
            loggedInMember = "-1"
            tbrViewCart.Enabled = False
        End If

        If (result = DialogResult.No) Then
            mnuMember.Enabled = True
        End If
    End Sub

    Public Sub closeCart()
        'Name:  closeCart()
        'Purpose: Changes properties when called in cart form's dispose
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        mnuViewCart.Enabled = True
        mnuBrowse.Enabled = True
        mnuCheckout.Enabled = False
        tbrViewCart.Enabled = True
    End Sub

    Public Sub closeBooks()
        'Name:  closeBooks()
        'Purpose: Changes properties when called in EBook form's dispose
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        mnuCheckout.Enabled = False
        mnuBrowse.Enabled = True
        tbrBrowse.Enabled = True
    End Sub


    Private Sub mnuBrowse_Click(sender As Object, e As EventArgs) Handles mnuBrowse.Click
        'Name:  mnuBrowse_Click
        'Purpose: Opens up EBooks form, sets mdi parent, and disables apropriate buttons
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim formBooks As New frmEbook
        formBooks.MdiParent = Me
        mnuBrowse.Enabled = False
        tbrBrowse.Enabled = False
        formBooks.Show()

    End Sub

    Private Sub mnuViewCart_Click(sender As Object, e As EventArgs) Handles mnuViewCart.Click
        'Name:  mnuViewCart_Click
        'Purpose: Opens up Cart form and sets the mdi parent and disables viewCart menu
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim formCart As frmCart
        formCart = New frmCart
        formCart.MdiParent = Me
        mnuViewCart.Enabled = False
        formCart.Show()

    End Sub

    
    Private Sub mnuDelAcc_Click(sender As Object, e As EventArgs) Handles mnuDelAcc.Click
        'Name:  mnuDelAcc_Click
        'Purpose: Opens up Members form and makes buttons visible to delete account
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim formDelete As New frmMember
        formDelete.MdiParent = Me
        formDelete.btnDelAccount.Visible = True
        formDelete.btnMakePayment.Visible = False
        formDelete.txtPassword.Visible = False
        formDelete.txtRPassword.Visible = False
        formDelete.Show()

    End Sub

    Private Sub tbrBrowse_Click(sender As Object, e As EventArgs) Handles tbrBrowse.Click
        'Name:  tbrBrowse_Click
        'Purpose: Performs mnu browses click
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        mnuBrowse.PerformClick()
    End Sub

    Private Sub mnuCheckout_Click(sender As Object, e As EventArgs) Handles mnuCheckout.Click
        'Name: mnuCheckout_Click
        'Purpose: makes queries to look at the users cart and check out increasing their balance
        'Parameters: sender As Object, e As EventArgs
        'Return: none

        Dim finalTotal As Double = total
        Dim counter As Integer = 0

        membersQuery = "select * from Members where IDNumber = '" & loggedInMember & "'"
        retrieveTable(membersQuery, MembersDT)
        If MembersDT.Rows(0).Item("MemberLevel") = "Gold" Then
            finalTotal = finalTotal - (finalTotal * 0.1)
            MembersDT.Rows(0).Item("Balance") = finalTotal + MembersDT.Rows(0).Item("Balance")
            updateTable(membersQuery, MembersDT)
        Else
            finalTotal = total
            MembersDT.Rows(0).Item("Balance") = finalTotal + MembersDT.Rows(0).Item("Balance")
            updateTable(membersQuery, MembersDT)
        End If
        cartQuery = "select * from ShoppingCarts where IDNumber = '" & loggedInMember & "'"
        retrieveTable(cartQuery, CartDT)
        While counter < CartDT.Rows.Count
            CartDT.Rows(counter).Delete()
            counter = counter + 1
        End While
        updateTable(cartQuery, CartDT)
        mnuCheckout.Enabled = False
        finalTotal = MembersDT.Rows(0).Item("Balance")

        If cartLoaded = True Then
            MessageBox.Show("Order Placed! Your Curent balance is " & FormatCurrency(finalTotal))
            daddySayBye = True
            Try
                For Each f As Form In My.Application.OpenForms
                    If f.Name = "frmCart" Or f.Name = "formCart" Then
                        f.Close()
                    End If
                    ' For i = System.Windows.Forms.Application.OpenForms.Count - 1 To 1 Step -1
                    'Dim form As Form = System.Windows.Forms.Application.OpenForms(i)

                Next
            Catch ex As Exception

            End Try
        Else
            MessageBox.Show("Your cart is empty already!")
        End If
        daddySayBye = False
        closeCart()
        total = 0
    End Sub
End Class