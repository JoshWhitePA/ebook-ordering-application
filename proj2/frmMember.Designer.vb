﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtBalance = New System.Windows.Forms.TextBox()
        Me.txtPayment = New System.Windows.Forms.TextBox()
        Me.radGold = New System.Windows.Forms.RadioButton()
        Me.radSilver = New System.Windows.Forms.RadioButton()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.txtRPassword = New System.Windows.Forms.TextBox()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.lblBalance = New System.Windows.Forms.Label()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.lblRPassword = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.grpMemberType = New System.Windows.Forms.GroupBox()
        Me.btnCreateAccount = New System.Windows.Forms.Button()
        Me.btnDelAccount = New System.Windows.Forms.Button()
        Me.btnMakePayment = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.txtPhoneNum = New System.Windows.Forms.MaskedTextBox()
        Me.grpMemberType.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(40, 61)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(218, 20)
        Me.txtID.TabIndex = 0
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(40, 107)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(218, 20)
        Me.txtLastName.TabIndex = 1
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(285, 107)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(168, 20)
        Me.txtFirstName.TabIndex = 2
        '
        'txtBalance
        '
        Me.txtBalance.Location = New System.Drawing.Point(285, 230)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Size = New System.Drawing.Size(168, 20)
        Me.txtBalance.TabIndex = 5
        Me.txtBalance.Visible = False
        '
        'txtPayment
        '
        Me.txtPayment.Location = New System.Drawing.Point(285, 280)
        Me.txtPayment.Name = "txtPayment"
        Me.txtPayment.Size = New System.Drawing.Size(168, 20)
        Me.txtPayment.TabIndex = 6
        Me.txtPayment.Visible = False
        '
        'radGold
        '
        Me.radGold.AutoSize = True
        Me.radGold.Location = New System.Drawing.Point(14, 19)
        Me.radGold.Name = "radGold"
        Me.radGold.Size = New System.Drawing.Size(47, 17)
        Me.radGold.TabIndex = 7
        Me.radGold.TabStop = True
        Me.radGold.Text = "Gold"
        Me.radGold.UseVisualStyleBackColor = True
        '
        'radSilver
        '
        Me.radSilver.AutoSize = True
        Me.radSilver.Location = New System.Drawing.Point(14, 62)
        Me.radSilver.Name = "radSilver"
        Me.radSilver.Size = New System.Drawing.Size(51, 17)
        Me.radSilver.TabIndex = 8
        Me.radSilver.TabStop = True
        Me.radSilver.Text = "Silver"
        Me.radSilver.UseVisualStyleBackColor = True
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(509, 230)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(168, 20)
        Me.txtPassword.TabIndex = 9
        Me.txtPassword.UseSystemPasswordChar = True
        Me.txtPassword.Visible = False
        '
        'txtRPassword
        '
        Me.txtRPassword.Location = New System.Drawing.Point(509, 280)
        Me.txtRPassword.Name = "txtRPassword"
        Me.txtRPassword.Size = New System.Drawing.Size(168, 20)
        Me.txtRPassword.TabIndex = 10
        Me.txtRPassword.UseSystemPasswordChar = True
        Me.txtRPassword.Visible = False
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(37, 43)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(221, 13)
        Me.lblID.TabIndex = 11
        Me.lblID.Text = "ID Number- Must Be Less than 11 Characters"
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Location = New System.Drawing.Point(37, 91)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(58, 13)
        Me.lblLastName.TabIndex = 12
        Me.lblLastName.Text = "Last Name"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Location = New System.Drawing.Point(282, 91)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(57, 13)
        Me.lblFirstName.TabIndex = 13
        Me.lblFirstName.Text = "First Name"
        '
        'lblBalance
        '
        Me.lblBalance.AutoSize = True
        Me.lblBalance.Location = New System.Drawing.Point(282, 214)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(46, 13)
        Me.lblBalance.TabIndex = 14
        Me.lblBalance.Text = "Balance"
        Me.lblBalance.Visible = False
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Location = New System.Drawing.Point(282, 43)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(78, 13)
        Me.lblPhone.TabIndex = 15
        Me.lblPhone.Text = "Phone Number"
        '
        'lblPayment
        '
        Me.lblPayment.AutoSize = True
        Me.lblPayment.Location = New System.Drawing.Point(282, 264)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(48, 13)
        Me.lblPayment.TabIndex = 16
        Me.lblPayment.Text = "Payment"
        Me.lblPayment.Visible = False
        '
        'lblRPassword
        '
        Me.lblRPassword.AutoSize = True
        Me.lblRPassword.Location = New System.Drawing.Point(506, 264)
        Me.lblRPassword.Name = "lblRPassword"
        Me.lblRPassword.Size = New System.Drawing.Size(97, 13)
        Me.lblRPassword.TabIndex = 17
        Me.lblRPassword.Text = "Re-enter Password"
        Me.lblRPassword.Visible = False
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(506, 214)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(162, 13)
        Me.lblPassword.TabIndex = 18
        Me.lblPassword.Text = "Password - Must be 8 characters"
        Me.lblPassword.Visible = False
        '
        'grpMemberType
        '
        Me.grpMemberType.Controls.Add(Me.radGold)
        Me.grpMemberType.Controls.Add(Me.radSilver)
        Me.grpMemberType.Location = New System.Drawing.Point(509, 43)
        Me.grpMemberType.Name = "grpMemberType"
        Me.grpMemberType.Size = New System.Drawing.Size(115, 84)
        Me.grpMemberType.TabIndex = 19
        Me.grpMemberType.TabStop = False
        Me.grpMemberType.Text = "Membership Type"
        '
        'btnCreateAccount
        '
        Me.btnCreateAccount.Location = New System.Drawing.Point(473, 323)
        Me.btnCreateAccount.Name = "btnCreateAccount"
        Me.btnCreateAccount.Size = New System.Drawing.Size(97, 23)
        Me.btnCreateAccount.TabIndex = 20
        Me.btnCreateAccount.Text = "Create Account"
        Me.btnCreateAccount.UseVisualStyleBackColor = True
        Me.btnCreateAccount.Visible = False
        '
        'btnDelAccount
        '
        Me.btnDelAccount.Location = New System.Drawing.Point(469, 323)
        Me.btnDelAccount.Name = "btnDelAccount"
        Me.btnDelAccount.Size = New System.Drawing.Size(101, 23)
        Me.btnDelAccount.TabIndex = 21
        Me.btnDelAccount.Text = "Delete Account"
        Me.btnDelAccount.UseVisualStyleBackColor = True
        Me.btnDelAccount.Visible = False
        '
        'btnMakePayment
        '
        Me.btnMakePayment.Location = New System.Drawing.Point(473, 323)
        Me.btnMakePayment.Name = "btnMakePayment"
        Me.btnMakePayment.Size = New System.Drawing.Size(97, 23)
        Me.btnMakePayment.TabIndex = 22
        Me.btnMakePayment.Text = "Make Payment"
        Me.btnMakePayment.UseVisualStyleBackColor = True
        Me.btnMakePayment.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(580, 323)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(97, 23)
        Me.btnCancel.TabIndex = 23
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'txtPhoneNum
        '
        Me.txtPhoneNum.Location = New System.Drawing.Point(285, 59)
        Me.txtPhoneNum.Mask = "(999) 000-0000"
        Me.txtPhoneNum.Name = "txtPhoneNum"
        Me.txtPhoneNum.Size = New System.Drawing.Size(168, 20)
        Me.txtPhoneNum.TabIndex = 26
        '
        'frmMember
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 424)
        Me.Controls.Add(Me.txtPhoneNum)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnMakePayment)
        Me.Controls.Add(Me.btnDelAccount)
        Me.Controls.Add(Me.btnCreateAccount)
        Me.Controls.Add(Me.grpMemberType)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblRPassword)
        Me.Controls.Add(Me.lblPayment)
        Me.Controls.Add(Me.lblPhone)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.lblFirstName)
        Me.Controls.Add(Me.lblLastName)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.txtRPassword)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtPayment)
        Me.Controls.Add(Me.txtBalance)
        Me.Controls.Add(Me.txtFirstName)
        Me.Controls.Add(Me.txtLastName)
        Me.Controls.Add(Me.txtID)
        Me.Name = "frmMember"
        Me.Text = "Members"
        Me.grpMemberType.ResumeLayout(False)
        Me.grpMemberType.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents txtPayment As System.Windows.Forms.TextBox
    Friend WithEvents radGold As System.Windows.Forms.RadioButton
    Friend WithEvents radSilver As System.Windows.Forms.RadioButton
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtRPassword As System.Windows.Forms.TextBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents lblRPassword As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents grpMemberType As System.Windows.Forms.GroupBox
    Friend WithEvents btnCreateAccount As System.Windows.Forms.Button
    Friend WithEvents btnDelAccount As System.Windows.Forms.Button
    Friend WithEvents btnMakePayment As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtPhoneNum As System.Windows.Forms.MaskedTextBox
End Class
