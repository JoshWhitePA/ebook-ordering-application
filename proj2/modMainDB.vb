﻿Module modMainDB
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: Allows acces to database, retrieve tables and update table to relfect user input
    Private Const DatabaseFile As String = "ebooks.accdb"
    Public Const c_connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;" & _
                                    "Data Source = " & DatabaseFile


    Public Sub retrieveTable(ByVal query As String, ByRef memoryTable As DataTable)
        'Name:  retrieveTable
        'Purpose: Gets table from database
        'Parameters: ByVal query As String, ByRef memoryTable As DataTable
        'Return: none

        Try
            Dim aTableAdapter As New OleDb.OleDbDataAdapter(query, c_connectionString)
            memoryTable.Clear()
            aTableAdapter.Fill(memoryTable)
            aTableAdapter.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "RETRIEVAL ERROR")
        End Try

    End Sub

    Public Sub updateTable(ByVal query As String, ByRef memoryTable As DataTable)
        'Name:  updateTable
        'Purpose: takes data from DT and applies it to main database
        'Parameters: ByVal query As String, ByRef memoryTable As DataTable
        'Return: none
        Try

            Dim aTableAdapter As New OleDb.OleDbDataAdapter(query, c_connectionString)
            Dim aCommBuilder As New OleDb.OleDbCommandBuilder(aTableAdapter)
            aTableAdapter.Update(memoryTable)
            aCommBuilder.Dispose()
            aTableAdapter.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message(), "UPDATE ERROR")
        End Try

    End Sub
End Module
