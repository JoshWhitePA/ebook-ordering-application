﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEbook
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSelectCategory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMandC = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFiction = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBiography = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOther = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAddtoCart = New System.Windows.Forms.ToolStripMenuItem()
        Me.dgvCart = New System.Windows.Forms.DataGridView()
        Me.mnuMain.SuspendLayout()
        CType(Me.dgvCart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuMain
        '
        Me.mnuMain.AllowMerge = False
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuClose, Me.mnuSelectCategory, Me.mnuAddtoCart})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(947, 24)
        Me.mnuMain.TabIndex = 0
        Me.mnuMain.Text = "mnuMain"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(48, 20)
        Me.mnuClose.Text = "Close"
        '
        'mnuSelectCategory
        '
        Me.mnuSelectCategory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAll, Me.mnuMandC, Me.mnuFiction, Me.mnuBiography, Me.mnuOther})
        Me.mnuSelectCategory.Name = "mnuSelectCategory"
        Me.mnuSelectCategory.Size = New System.Drawing.Size(101, 20)
        Me.mnuSelectCategory.Text = "Select Category"
        '
        'mnuMandC
        '
        Me.mnuMandC.Name = "mnuMandC"
        Me.mnuMandC.Size = New System.Drawing.Size(174, 22)
        Me.mnuMandC.Text = "Mystery and Crime"
        '
        'mnuFiction
        '
        Me.mnuFiction.Name = "mnuFiction"
        Me.mnuFiction.Size = New System.Drawing.Size(174, 22)
        Me.mnuFiction.Text = "Fiction"
        '
        'mnuBiography
        '
        Me.mnuBiography.Name = "mnuBiography"
        Me.mnuBiography.Size = New System.Drawing.Size(174, 22)
        Me.mnuBiography.Text = "Biography"
        '
        'mnuAll
        '
        Me.mnuAll.Name = "mnuAll"
        Me.mnuAll.Size = New System.Drawing.Size(174, 22)
        Me.mnuAll.Text = "All"
        '
        'mnuOther
        '
        Me.mnuOther.Name = "mnuOther"
        Me.mnuOther.Size = New System.Drawing.Size(174, 22)
        Me.mnuOther.Text = "Other"
        '
        'mnuAddtoCart
        '
        Me.mnuAddtoCart.Enabled = False
        Me.mnuAddtoCart.Name = "mnuAddtoCart"
        Me.mnuAddtoCart.Size = New System.Drawing.Size(80, 20)
        Me.mnuAddtoCart.Text = "Add to Cart"
        '
        'dgvCart
        '
        Me.dgvCart.AllowUserToOrderColumns = True
        Me.dgvCart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        Me.dgvCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCart.Location = New System.Drawing.Point(35, 40)
        Me.dgvCart.Name = "dgvCart"
        Me.dgvCart.Size = New System.Drawing.Size(890, 379)
        Me.dgvCart.TabIndex = 1
        '
        'frmEbook
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(947, 450)
        Me.Controls.Add(Me.dgvCart)
        Me.Controls.Add(Me.mnuMain)
        Me.MainMenuStrip = Me.mnuMain
        Me.Name = "frmEbook"
        Me.Text = "E-Books"
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        CType(Me.dgvCart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCategory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMandC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFiction As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBiography As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOther As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAddtoCart As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvCart As System.Windows.Forms.DataGridView
End Class
