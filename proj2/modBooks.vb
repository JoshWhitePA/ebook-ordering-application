﻿Module modBooks
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: Searches the Books datatable for a specfic item
    Public Const BookTable As String = "Books"
    Public bookQuery As String
    Public BookDT As New DataTable



    Public Function searchBookBYISBN(ByVal id As String) As Integer
        'Name: searchBookBYISBN
        'Purpose: Searches the Books datatable for a specfic ISBN
        'Parameters: ByVal id As String
        'Return: Returns the row number if found
        'Returns -1 if not found

        Dim i As Integer = 0
        Dim found As Boolean = False

        While i < BookDT.Rows.Count And Not found
            If BookDT.Rows(i).RowState = DataRowState.Deleted Then
                i += 1
            Else
                If BookDT.Rows(i).Item("ISBN") = id Then
                    found = True
                Else
                    i += 1
                End If
            End If
        End While
        If found Then
            Return i
        Else
            Return -1
        End If
    End Function

    Public Function searchBookBYGenre(ByVal id As String) As Integer
        'Name: searchBookBYGenre
        'Purpose: Searches the Books datatable for a specfic Genre
        'Parameters: ByVal id As String
        'Return: Returns the row number if found
        'Returns -1 if not found

        Dim i As Integer = 0
        Dim found As Boolean = False

        While i < BookDT.Rows.Count And Not found
            If BookDT.Rows(i).RowState = DataRowState.Deleted Then
                i += 1
            Else
                If BookDT.Rows(i).Item("Genre") = id Then
                    found = True
                Else
                    i += 1
                End If
            End If
        End While
        If found Then
            Return i
        Else
            Return -1
        End If
    End Function

End Module
