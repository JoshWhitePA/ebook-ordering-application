﻿Public Class frmContactUs
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: To allow user to shop for e-books and contact the owner of program
    Dim myParent As frmMain
    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        'Name: btnSubmit_Click
        'Purpose: Checks user input for incomplete fields, disposes of form
        'Parameters: sender As Object, e As EventArgs
        'Returns: None
        If txtName.Text = "" Then
            MessageBox.Show("You need to enter your name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)

        ElseIf txtEmail.Text = "" Then
            MessageBox.Show("You need to enter your Email", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)

        ElseIf txtSubject.Text = "" Then
            MessageBox.Show("You need to include a subject", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)

        ElseIf txtMessage.Text = "" Then
            MessageBox.Show("You need to include a message", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End If
        'If none of the inputs are empty then submit would close the file
        If txtName.Text <> "" And txtEmail.Text <> "" And txtSubject.Text <> "" And txtMessage.Text <> "" Then
            Me.Dispose()
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        'Name:  btnCancel_Click
        'Purpose:  disposes when user hit cancel button
        'Parameters: sender As Object, e As EventArgs
        'Returns: None

        Me.Dispose()
    End Sub

End Class