﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.mnuLogin = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEA = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCA = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMember = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMakePay = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDelAcc = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLogout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEBooks = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBrowse = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuShopCart = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuViewCart = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCheckout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContact = New System.Windows.Forms.ToolStripMenuItem()
        Me.tbrMain = New System.Windows.Forms.ToolStrip()
        Me.tbrLogin = New System.Windows.Forms.ToolStripButton()
        Me.tbrExit = New System.Windows.Forms.ToolStripButton()
        Me.tbrViewCart = New System.Windows.Forms.ToolStripButton()
        Me.tbrBrowse = New System.Windows.Forms.ToolStripButton()
        Me.sbrMain = New System.Windows.Forms.StatusStrip()
        Me.tbrBookPic = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbrWelcome = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbrTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbrDate = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tmrTick = New System.Windows.Forms.Timer(Me.components)
        Me.mnuMain.SuspendLayout()
        Me.tbrMain.SuspendLayout()
        Me.sbrMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLogin, Me.mnuMember, Me.mnuEBooks, Me.mnuShopCart, Me.mnuContact})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mnuMain.Size = New System.Drawing.Size(846, 24)
        Me.mnuMain.TabIndex = 0
        Me.mnuMain.Text = "MenuStrip1"
        '
        'mnuLogin
        '
        Me.mnuLogin.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEA, Me.mnuCA})
        Me.mnuLogin.Name = "mnuLogin"
        Me.mnuLogin.Size = New System.Drawing.Size(49, 20)
        Me.mnuLogin.Text = "Login"
        '
        'mnuEA
        '
        Me.mnuEA.Name = "mnuEA"
        Me.mnuEA.Size = New System.Drawing.Size(162, 22)
        Me.mnuEA.Text = "Existing Account"
        '
        'mnuCA
        '
        Me.mnuCA.Name = "mnuCA"
        Me.mnuCA.Size = New System.Drawing.Size(162, 22)
        Me.mnuCA.Text = "Create Account"
        '
        'mnuMember
        '
        Me.mnuMember.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMakePay, Me.mnuDelAcc, Me.mnuLogout})
        Me.mnuMember.Enabled = False
        Me.mnuMember.Name = "mnuMember"
        Me.mnuMember.Size = New System.Drawing.Size(64, 20)
        Me.mnuMember.Text = "Member"
        '
        'mnuMakePay
        '
        Me.mnuMakePay.Name = "mnuMakePay"
        Me.mnuMakePay.Size = New System.Drawing.Size(155, 22)
        Me.mnuMakePay.Text = "Make Payment"
        '
        'mnuDelAcc
        '
        Me.mnuDelAcc.Name = "mnuDelAcc"
        Me.mnuDelAcc.Size = New System.Drawing.Size(155, 22)
        Me.mnuDelAcc.Text = "Delete Account"
        '
        'mnuLogout
        '
        Me.mnuLogout.Name = "mnuLogout"
        Me.mnuLogout.Size = New System.Drawing.Size(155, 22)
        Me.mnuLogout.Text = "Logout"
        '
        'mnuEBooks
        '
        Me.mnuEBooks.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBrowse})
        Me.mnuEBooks.Name = "mnuEBooks"
        Me.mnuEBooks.Size = New System.Drawing.Size(57, 20)
        Me.mnuEBooks.Text = "EBooks"
        '
        'mnuBrowse
        '
        Me.mnuBrowse.Name = "mnuBrowse"
        Me.mnuBrowse.Size = New System.Drawing.Size(112, 22)
        Me.mnuBrowse.Text = "Browse"
        '
        'mnuShopCart
        '
        Me.mnuShopCart.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewCart, Me.mnuCheckout})
        Me.mnuShopCart.Enabled = False
        Me.mnuShopCart.Name = "mnuShopCart"
        Me.mnuShopCart.Size = New System.Drawing.Size(95, 20)
        Me.mnuShopCart.Text = "Shopping Cart"
        '
        'mnuViewCart
        '
        Me.mnuViewCart.Name = "mnuViewCart"
        Me.mnuViewCart.Size = New System.Drawing.Size(125, 22)
        Me.mnuViewCart.Text = "View Cart"
        '
        'mnuCheckout
        '
        Me.mnuCheckout.Enabled = False
        Me.mnuCheckout.Name = "mnuCheckout"
        Me.mnuCheckout.Size = New System.Drawing.Size(125, 22)
        Me.mnuCheckout.Text = "Checkout"
        '
        'mnuContact
        '
        Me.mnuContact.Name = "mnuContact"
        Me.mnuContact.Size = New System.Drawing.Size(77, 20)
        Me.mnuContact.Text = "Contact Us"
        '
        'tbrMain
        '
        Me.tbrMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbrLogin, Me.tbrExit, Me.tbrViewCart, Me.tbrBrowse})
        Me.tbrMain.Location = New System.Drawing.Point(0, 24)
        Me.tbrMain.Name = "tbrMain"
        Me.tbrMain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbrMain.Size = New System.Drawing.Size(846, 25)
        Me.tbrMain.TabIndex = 1
        Me.tbrMain.Text = "ToolBar"
        '
        'tbrLogin
        '
        Me.tbrLogin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbrLogin.Image = CType(resources.GetObject("tbrLogin.Image"), System.Drawing.Image)
        Me.tbrLogin.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbrLogin.Name = "tbrLogin"
        Me.tbrLogin.Size = New System.Drawing.Size(23, 22)
        Me.tbrLogin.Text = "Login"
        '
        'tbrExit
        '
        Me.tbrExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbrExit.Image = CType(resources.GetObject("tbrExit.Image"), System.Drawing.Image)
        Me.tbrExit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbrExit.Name = "tbrExit"
        Me.tbrExit.Size = New System.Drawing.Size(23, 22)
        Me.tbrExit.Text = "Exit"
        '
        'tbrViewCart
        '
        Me.tbrViewCart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbrViewCart.Enabled = False
        Me.tbrViewCart.Image = CType(resources.GetObject("tbrViewCart.Image"), System.Drawing.Image)
        Me.tbrViewCart.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbrViewCart.Name = "tbrViewCart"
        Me.tbrViewCart.Size = New System.Drawing.Size(23, 22)
        Me.tbrViewCart.Text = "View Cart"
        '
        'tbrBrowse
        '
        Me.tbrBrowse.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbrBrowse.Image = CType(resources.GetObject("tbrBrowse.Image"), System.Drawing.Image)
        Me.tbrBrowse.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbrBrowse.Name = "tbrBrowse"
        Me.tbrBrowse.Size = New System.Drawing.Size(23, 22)
        Me.tbrBrowse.Text = "Browse"
        '
        'sbrMain
        '
        Me.sbrMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbrBookPic, Me.sbrWelcome, Me.sbrTime, Me.sbrDate})
        Me.sbrMain.Location = New System.Drawing.Point(0, 393)
        Me.sbrMain.Name = "sbrMain"
        Me.sbrMain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.sbrMain.Size = New System.Drawing.Size(846, 22)
        Me.sbrMain.TabIndex = 3
        Me.sbrMain.Text = "Status"
        '
        'tbrBookPic
        '
        Me.tbrBookPic.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me.tbrBookPic.Image = CType(resources.GetObject("tbrBookPic.Image"), System.Drawing.Image)
        Me.tbrBookPic.Name = "tbrBookPic"
        Me.tbrBookPic.Size = New System.Drawing.Size(16, 17)
        '
        'sbrWelcome
        '
        Me.sbrWelcome.Name = "sbrWelcome"
        Me.sbrWelcome.Size = New System.Drawing.Size(750, 17)
        Me.sbrWelcome.Spring = True
        '
        'sbrTime
        '
        Me.sbrTime.Name = "sbrTime"
        Me.sbrTime.Size = New System.Drawing.Size(34, 17)
        Me.sbrTime.Text = "Time"
        '
        'sbrDate
        '
        Me.sbrDate.Name = "sbrDate"
        Me.sbrDate.Size = New System.Drawing.Size(31, 17)
        Me.sbrDate.Text = "Date"
        '
        'tmrTick
        '
        Me.tmrTick.Enabled = True
        Me.tmrTick.Interval = 1000
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 415)
        Me.Controls.Add(Me.sbrMain)
        Me.Controls.Add(Me.tbrMain)
        Me.Controls.Add(Me.mnuMain)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.mnuMain
        Me.Name = "frmMain"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Order EBooks"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.tbrMain.ResumeLayout(False)
        Me.tbrMain.PerformLayout()
        Me.sbrMain.ResumeLayout(False)
        Me.sbrMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuLogin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMember As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMakePay As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelAcc As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLogout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEBooks As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShopCart As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewCart As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCheckout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuContact As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbrMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tbrLogin As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbrExit As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbrViewCart As System.Windows.Forms.ToolStripButton
    Friend WithEvents sbrMain As System.Windows.Forms.StatusStrip
    Friend WithEvents tbrBookPic As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents sbrTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents sbrDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tmrTick As System.Windows.Forms.Timer
    Friend WithEvents tbrBrowse As System.Windows.Forms.ToolStripButton
    Friend WithEvents sbrWelcome As System.Windows.Forms.ToolStripStatusLabel

End Class
