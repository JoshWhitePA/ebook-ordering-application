﻿Public Class frmMember
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: To allow user to shop for e-books and contact the owner of program
    Dim myDaddy As frmMain
    Dim memberQuery As String = "Select * from Members"
    Dim Index As Integer
    Dim balance As Double

    Private Sub frmMember_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        'Name: frmMember_Disposed
        'Purpose: makes menu items enabled if form is disposed
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        If btnCreateAccount.Visible = True Then
            myDaddy.mnuMember.Enabled = False
        End If
        If btnCreateAccount.Visible = False Then
            myDaddy.mnuMember.Enabled = True
        End If

    End Sub

    Private Sub frmMember_HandleDestroyed(sender As Object, e As EventArgs) Handles Me.HandleDestroyed
        'Name: frmMember_HandleDestroyed
        'Purpose: makes menu items enabled if form destroyed
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        If LoggedIn = False Then
            myDaddy.mnuCA.Enabled = True
        End If
    End Sub

    Private Sub frmMember_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Name: frmMember_Load
        'Purpose: Sets the forms Mdi PArent to frmMain
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        radSilver.Checked = True
        myDaddy = Me.MdiParent
        Dim idrow As DataRow
        Try
            memberQuery = "Select * from Members where IDNumber = '" & loggedInMember & "'"
            retrieveTable(memberQuery, MembersDT)
            Dim idx As Integer = searchMembersBYID(loggedInMember)
            idrow = MembersDT.Rows(idx)
            copyRecordToForm(idrow)
            Index = idx
        Catch
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        'Name: btnCancel_Click
        'Purpose: Enables the create account button and disposes of form
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        If LoggedIn = False Then
            myDaddy.mnuCA.Enabled = True
        End If
        Me.Dispose()
    End Sub

    Private Sub btnCreateAccount_Click(sender As Object, e As EventArgs) Handles btnCreateAccount.Click
        'Name: btnCreateAccount_Click
        'Purpose: adds person to database, then messagebox saying the account was created and disposes of form
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        If txtLastName.Text = "" Or txtFirstName.Text = "" Or txtID.Text = "" Or txtPassword.Text = "" Or txtRPassword.Text = "" Or Len(txtPassword.Text) <> 8 Then
            MessageBox.Show("All fields were not filled in corectly, please fill in all fields.")
            Exit Sub
        End If

        If txtPassword.Text <> txtRPassword.Text Then
            MessageBox.Show("Passwords do not match!")
            txtPassword.Text = ""
            txtRPassword.Text = ""
            txtPassword.Focus()
            Exit Sub
        End If
        If txtLastName.Text <> "" And txtFirstName.Text <> "" And txtID.Text <> "" And txtPassword.Text <> "" And txtPassword.Text = txtRPassword.Text Then
            Dim rowId As DataRow
            rowId = MembersDT.NewRow '1. Create a new row
            copyFormToRecordCA(rowId) '2. Copy values to row
            MembersDT.Rows.Add(rowId) '3. Add the row to dataset
            updateTable(memberQuery, MembersDT)
            myDaddy.mnuCA.Enabled = True
            MessageBox.Show("Account Created!", "Created")
            Me.Dispose()
        End If


        


    End Sub

    Private Sub btnDelAccount_Click(sender As Object, e As EventArgs) Handles btnDelAccount.Click
        'Name: btnDelAccount_Click
        'Purpose: Shows messagebox asking if the account should be deleted, if yes then the form is disposed and account is deleted
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Try
            Dim result = MessageBox.Show("Are you sure you want to delete your account?", "Delete", MessageBoxButtons.YesNo)
            If (result = DialogResult.Yes) Then
                MembersDT.Rows(Index).Delete()
                updateTable(memberQuery, MembersDT)
                MessageBox.Show("Account Deleted")
                myDaddy.mnuCA.Enabled = True
                myDaddy.mnuLogout.PerformClick()
                Me.Dispose()
            End If
        Catch
        End Try
    End Sub

    Private Sub btnMakePayment_Click(sender As Object, e As EventArgs) Handles btnMakePayment.Click
        'Name: btnMakePayment_Click
        'Purpose: disposes of form
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Try
            Dim boolIR As Boolean
            balance = CDbl(MembersDT.Rows(Index).Item("Balance"))
            Dim idrow As DataRow
            Dim payBox As Double
            idrow = MembersDT.Rows(Index)
            boolIR = IsNumeric(txtPayment.Text)
            If boolIR = False Then
                MessageBox.Show("That input was not Numeric")
                txtPayment.Text = ""
                Exit Sub
            End If

            payBox = CDbl(txtPayment.Text)

            If payBox > balance Then
                MessageBox.Show("That number was greater than your balance")
            End If

            If boolIR = True And payBox <= balance Then

                balance = balance - payBox
                If balance = 0 Then
                    balance = 0.0
                End If
                txtBalance.Text = balance

                copyFormToRecord(idrow)
                updateTable(memberQuery, MembersDT)
                Me.Dispose()
            End If
        Catch
        End Try

    End Sub

    Private Sub copyRecordToForm(ByVal aRow As DataRow)
        'Name: copyRecordToForm
        'Purpose: Copies the data row to the text boxes
        'Parameters: ByVal aRow As DataRow
        'Return: none
        Try
            With aRow
                txtID.Text = .Item("IDNumber")
                txtLastName.Text = .Item("LastName")
                txtFirstName.Text = .Item("FirstName")
                txtPhoneNum.Text = .Item("PhoneNumber")
                txtBalance.Text = FormatCurrency(.Item("Balance"))
                txtPassword.Text = .Item("Passwrd")

                If .Item("MemberLevel") = "Gold" Then
                    radGold.Checked = True
                Else
                    radSilver.Checked = True
                End If
            End With
        Catch
        End Try
    End Sub

    Private Sub copyFormToRecord(ByRef aRow As DataRow)
        'Name: copyFormToRecord
        'Purpose: Copies the Form info to reccord
        'Parameters: ByRef aRow As DataRow
        'Return: none
        Try
            With aRow
                .Item("IDNumber") = txtID.Text
                .Item("LastName") = txtLastName.Text
                .Item("FirstName") = txtFirstName.Text
                .Item("PhoneNumber") = txtPhoneNum.Text
                If txtPayment.Text = "" Then
                    .Item("Balance") = txtBalance.Text
                Else
                    .Item("Balance") = balance
                End If

                .Item("Passwrd") = txtPassword.Text
                If radGold.Checked = True Then
                    .Item("MemberLevel") = "Gold"
                Else
                    .Item("MemberLevel") = "Standard"
                End If
            End With
        Catch
        End Try
    End Sub

    Private Sub copyFormToRecordCA(ByRef aRow As DataRow)
        'Name: copyFormToRecordCA
        'Purpose: Copies the Form info to reccord
        'Parameters: ByRef aRow As DataRow
        'Return: none
        Try
            With aRow
                .Item("IDNumber") = txtID.Text
                .Item("LastName") = txtLastName.Text
                .Item("FirstName") = txtFirstName.Text
                .Item("PhoneNumber") = txtPhoneNum.Text
                .Item("Balance") = 0
                .Item("Passwrd") = txtPassword.Text
                    If radGold.Checked = True Then
                        .Item("MemberLevel") = "Gold"
                    Else
                        .Item("MemberLevel") = "Standard"
                    End If
            End With
        Catch

        End Try

    End Sub

End Class