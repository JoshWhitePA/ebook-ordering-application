﻿Public Class frmCart
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: To allow user to see what is their cart

    Dim myDaddy As frmMain


    Private Sub frmCart_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        'Name: frmCart_Disposed
        'Purpose: Calls closeCart from parent
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        If daddySayBye <> True Then
            myDaddy.closeCart()
        End If
        Me.Close()

    End Sub



    Private Sub frmCart_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Name: frmCart_Load
        'Purpose: Populates and formats listbox, sets mdi parent
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim counter As Integer = 0
        myDaddy = Me.MdiParent

        Dim format As String = "{0,-20}{1,-60}{2,10:C}"
        Dim str2 As String = "{0,-20}{1,-60}{2,10}"


        comboQuery = "select Books.Title,Books.Price,Books.ISBN from ShoppingCarts, Books, Members where ShoppingCarts.IDNumber = '" & loggedInMember & "' and ShoppingCarts.ISBN = Books.ISBN and Members.IDNumber = ShoppingCarts.IDNumber"
        retrieveTable(comboQuery, comboDT)
        lstCart.Items.Add(String.Format(str2, "ISBN", "Title", "Price"))

        While counter < comboDT.Rows.Count
            Dim ISBN, Title As String
            Dim Price As Double
            Title = comboDT.Rows(counter).Item("Title")
            ISBN = comboDT.Rows(counter).Item("ISBN")
            Price = comboDT.Rows(counter).Item("Price")
            lstCart.Items.Add(String.Format(format, ISBN, Title, Price))

            counter = counter + 1
            total = Price + total
        End While
        txtTotal.Text = total
        myDaddy.mnuCheckout.Enabled = True
        If lstCart.Items.Count > 1 Then
            cartLoaded = True
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        'Name: btnClose_Click
        'Purpose: Disposes form
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Me.Dispose()

    End Sub
   

End Class