﻿Module modMembers
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: Searches the Books datatable for a specfic ID or password

    Public Const BookTable As String = "Members"
    Public membersQuery As String
    Public MembersDT As New DataTable

    Public Function searchMembersBYID(ByVal id As String) As Integer
        'Name:  searchMembersBYID
        'Purpose: Searches the Books datatable for a specfic ID
        'Parameters: ByVal id As String
        'Return: Returns the row number if found
        'Returns -1 if not found
        Dim i As Integer = 0
        Dim found As Boolean = False

        While i < MembersDT.Rows.Count And Not found
            If MembersDT.Rows(i).RowState = DataRowState.Deleted Then
                i += 1
            Else
                If MembersDT.Rows(i).Item("IDNumber") = id Then
                    found = True
                Else
                    i += 1
                End If
            End If
        End While
        If found Then
            Return i
        Else
            Return -1
        End If
    End Function

    Public Function searchMembersBYPass(ByVal id As String) As Integer
        'Name:  searchMembersBYPass
        'Purpose: Searches the Books datatable for a specfic Password
        'Parameters: ByVal id As String
        'Return: Returns the row number if found
        'Returns -1 if not found

        Dim i As Integer = 0
        Dim found As Boolean = False

        While i < MembersDT.Rows.Count And Not found
            If MembersDT.Rows(i).RowState = DataRowState.Deleted Then
                i += 1
            Else
                If MembersDT.Rows(i).Item("Passwrd") = id Then
                    found = True
                Else
                    i += 1
                End If
            End If
        End While
        If found Then
            Return i
        Else
            Return -1
        End If
    End Function
End Module
