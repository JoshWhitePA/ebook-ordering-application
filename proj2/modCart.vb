﻿Module modCart

    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: USed for searching cart table
    Public Const CartTable As String = "ShoppingCarts"
    Public cartQuery As String
    Public CartDT As New DataTable


    Public Function searchCartBYID(ByVal id As String) As Integer
        'Name:  searchCartBYID
        'Purpose: Searches the Books datatable for a specfic ISBN
        'Parameters: ByVal id As String
        'Return:'Returns the row number if found Returns -1 if not found

        Dim i As Integer = 0
        Dim found As Boolean = False

        While i < CartDT.Rows.Count And Not found
            If CartDT.Rows(i).RowState = DataRowState.Deleted Then
                i += 1
            Else
                If CartDT.Rows(i).Item("ID") = id Then
                    found = True
                Else
                    i += 1
                End If
            End If
        End While
        If found Then
            Return i
        Else
            Return -1
        End If
    End Function
End Module
