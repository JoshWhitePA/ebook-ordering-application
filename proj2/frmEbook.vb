﻿Public Class frmEbook
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: To allow user to shop for e-books 
    Dim myDaddy As frmMain
    Dim cartQuery As String
    Dim ebookQuery As String

    Private Sub frmEbook_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        'Name:  frmEbook_Disposed
        'Purpose: Calls closeBooks from parents
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        myDaddy.closeBooks()
        total = 0
    End Sub

    Private Sub frmEbook_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Name:  frmEbook_Load
        'Purpose: Sets mdi parent and enables addtocart
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        If LoggedIn = False Then
            mnuAddtoCart.Enabled = False
        End If
        Try
            myDaddy = Me.MdiParent
            mnuAddtoCart.Enabled = True
            mnuAll.Checked = True

            If LoggedIn = False Then
                ebookQuery = "Select ISBN, Title, Author, NewItem, Price  from Books"
                display()
            End If

            If LoggedIn = True Then
                Try
                    cartQuery = "Select * from ShoppingCarts where IDNumber = '" & loggedInMember & "'"
                    mnuAddtoCart.Enabled = True
                    retrieveTable(cartQuery, CartDT)
                    dgvCart.DataSource = CartDT
                    dgvCart.AutoResizeColumns()
                Catch
                End Try
            End If
        Catch
        End Try
    End Sub

    Private Sub uncheckOptions()
        'Name: uncheckOptions()
        'Purpose: Unchecks all menu choices
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        mnuFiction.Checked = False
        mnuMandC.Checked = False
        mnuBiography.Checked = False
        mnuAll.Checked = False
        mnuOther.Checked = False

    End Sub

    Private Sub mnuMandC_Click(sender As Object, e As EventArgs) Handles mnuMandC.Click
        'Name: mnuMandC_Click
        'Purpose: checks mystery and crime
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        uncheckOptions()
        mnuMandC.Checked = True
        ebookQuery = "Select ISBN, Title, Author, NewItem, Price  from Books where Genre = 'mystery and crime'"
        display()
    End Sub

    Private Sub mnuOther_Click(sender As Object, e As EventArgs) Handles mnuOther.Click
        'Name: mnuOther_Click
        'Purpose: Shows inputbox and checks mnuOther
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Dim otherAnswer As String
        uncheckOptions()
        otherAnswer = InputBox("Please enter a category")
        mnuOther.Checked = True
        Try
            ebookQuery = "Select ISBN, Title, Author, NewItem, Price  from Books where Genre = '" & otherAnswer & "'"
            retrieveTable(ebookQuery, BookDT)
            If BookDT.Rows.Count = 0 Then
                MessageBox.Show("Genre Not Found")
                Exit Sub
            End If
            display()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub mnuClose_Click(sender As Object, e As EventArgs) Handles mnuClose.Click
        'Name: mnuClose_Click
        'Purpose: disposes form and enables parents browse
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        myDaddy.mnuBrowse.Enabled = True
        Me.Dispose()
    End Sub

    Sub display()
        'Name:  display()
        'Purpose: Displayes data table in dvg
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Try
            retrieveTable(ebookQuery, BookDT)
            dgvCart.DataSource = BookDT
            dgvCart.AutoResizeColumns()
            dgvCart.Columns.Item("Price").DefaultCellStyle.Format = "c"
            dgvCart.Columns.Item("Price").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch
        End Try
    End Sub

    Private Sub mnuAll_Click(sender As Object, e As EventArgs) Handles mnuAll.Click
        'Name:  mnuAll_Click
        'Purpose: Queries all books with selected colums showed
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        uncheckOptions()
        mnuAll.Checked = True
        ebookQuery = "Select ISBN, Title, Author, NewItem, Price  from Books"
        display()
    End Sub

    Private Sub mnuBiography_Click(sender As Object, e As EventArgs) Handles mnuBiography.Click
        'Name:  mnuBiography_Click
        'Purpose: Queries for books with biography as genre
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        uncheckOptions()
        mnuBiography.Checked = True
        ebookQuery = "Select ISBN, Title, Author, NewItem, Price  from Books where Genre = 'biography'"
        display()
    End Sub

    Private Sub mnuFiction_Click(sender As Object, e As EventArgs) Handles mnuFiction.Click
        'Name: mnuFiction_Click
        'Purpose: Queries for books with Fiction as genre
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        uncheckOptions()
        mnuFiction.Checked = True
        ebookQuery = "Select ISBN, Title, Author, NewItem, Price  from Books where Genre = 'fiction'"
        display()
    End Sub

    Private Sub mnuAddtoCart_Click(sender As Object, e As EventArgs) Handles mnuAddtoCart.Click
        'Name: mnuAddtoCart_Click
        'Purpose: Adds selected row to database
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        If LoggedIn = True Then
            Dim isbn As String = dgvCart.CurrentRow.Cells("ISBN").Value()
            Dim rowID As DataRow
            rowID = CartDT.NewRow ' Create a new row
            copyFormToRecord(rowID, isbn)
            CartDT.Rows.Add(rowID)
            updateTable(cartQuery, CartDT)
            MessageBox.Show("Book Added!")

        Else
            MessageBox.Show("You need to sign in to add to your cart!")
        End If
    End Sub

    Private Sub copyFormToRecord(ByRef aRow As DataRow, isbn As String)
        'Name: copyFormToRecord
        'Purpose: adds info to new row
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        With aRow
            .Item("IDNumber") = loggedInMember
            .Item("ISBN") = isbn
        End With
    End Sub
End Class