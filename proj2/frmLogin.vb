﻿Public Class frmLogin
    'Student:Joshua White
    'Instructor: Professor Day
    'Class:CSC 241
    'Purpose: Allows user to login to their account
    Dim myDaddy As frmMain
    Dim loggedInQuery As String


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        'Name: btnCancel_Click
        'Purpose: Disposes form
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Me.Dispose()
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        'Name:  btnLogin_Click
        'Purpose: Takes input and checks if credentails show they are a member
        'Parameters: sender As Object, e As EventArgs
        'Return: none
        Try
            Dim searchPass As Integer
            loggedInQuery = "Select * from Members where LastName = '" & txtLastName.Text & "'"
            retrieveTable(loggedInQuery, MembersDT)
            searchPass = searchMembersBYPass(txtPassword.Text)

            If MembersDT.Rows.Count = 0 Then
                txtLastName.Text = ""
                txtPassword.Text = ""
                MessageBox.Show("Sorry, your Last Name was not found", "Invalid")
                txtLastName.Focus()
                Exit Sub
            End If

            If searchPass = -1 Then
                txtPassword.Text = ""
                MessageBox.Show("Sorry, your Password was not found", "Invalid")
                txtPassword.Focus()
                Exit Sub
            End If

            If MembersDT.Rows.Count <> 0 And searchPass <> -1 Then
                LoggedIn = True
                copyRecordToLoggedIn(MembersDT.Rows(searchPass))
                MemberName = MembersDT.Rows(searchPass).Item("FirstName") & " " & txtLastName.Text
                ' LoggedInID = " where LastName = '" & txtLastName.Text & "' and Passwrd = '" & txtPassword.Text & "'"
                Me.Hide()
            End If
        Catch
        End Try

    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Name:  frmLogin_Load
        'Purpose: Sets mdi parent and focuses on textbox
        'Parameters: sender As Object, e As EventArgs
        'Return: none

        myDaddy = Me.MdiParent
        txtLastName.Focus()

    End Sub

    Private Sub copyRecordToLoggedIn(ByVal aRow As DataRow)
        'Name:  copyRecordToLoggedIn
        'Purpose: Sets the IDnumber of logged in person to loggedInMember
        'Parameters: ByVal aRow As DataRow
        'Return: none

        loggedInMember = aRow.Item("IDNumber")

    End Sub

End Class